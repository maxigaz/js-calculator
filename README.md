# Javascript Calculator

This is a simple calculator made for a freeCodeCamp project.

## How to test it

Clone and give it a try locally or go to the project's [demo page](https://maxigaz.gitlab.io/js-calculator/).

## Credits

jQuery https://jquery.com/, (c) 2005, 2018 jQuery Foundation, Inc., [MIT](https://jquery.org/license)
