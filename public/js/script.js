$(document).ready(function() {
	var inputNum = [];
	var operator = "";
	var cache = [];
	var result;

	var opList = {
		'+': function(a, b) { return parseFloat(a.join('')) + parseFloat(b.join('')); },
		'-': function(a, b) { return parseFloat(a.join('')) - parseFloat(b.join('')); },
		'*': function(a, b) { return parseFloat(a.join('')) * parseFloat(b.join('')); },
		'/': function(a, b) { return parseFloat(a.join('')) / parseFloat(b.join('')); }
	};

	var messageShown = false;

	// Add number as last digit, then turn it from a string into a real number
	$(".one").click(function() {
		updateScreen("1");
	});
	$(".two").click(function() {
		updateScreen("2");
	});
	$(".three").click(function() {
		updateScreen("3");
	});
	$(".four").click(function() {
		updateScreen("4");
	});
	$(".five").click(function() {
		updateScreen("5");
	});
	$(".six").click(function() {
		updateScreen("6");
	});
	$(".seven").click(function() {
		updateScreen("7");
	});
	$(".eight").click(function() {
		updateScreen("8");
	});
	$(".nine").click(function() {
		updateScreen("9");
	});
	$(".zero").click(function() {
		updateScreen("0");
	});
	$(".point").click(function() {
		updateScreen(".");
	});

	$(".plus").click(function() {
		cacheOp("+");
	});
	$(".minus").click(function() {
		cacheOp("-");
	});
	$(".multiply").click(function() {
		cacheOp("*");
	});
	$(".divide").click(function() {
		cacheOp("/");
	});

	$(".equal").click(function() {
		calculate();
	});

	$(".ac").click(function() {
		clearEverything();
	});
	$(".ce").click(function() {
		clearScreen();
	});

	// Key bindings
	$(document).keypress(function(e) {
		switch (e.which) {
				// Numbers from 0 to 9
			case 48:
			case 96:
				updateScreen("0");
				break;
			case 49:
			case 97:
				updateScreen("1");
				break;
			case 50:
			case 98:
				updateScreen("2");
				break;
			case 51:
			case 99:
				updateScreen("3");
				break;
			case 52:
			case 100:
				updateScreen("4");
				break;
			case 53:
			case 101:
				updateScreen("5");
				break;
			case 54:
			case 102:
				updateScreen("6");
				break;
			case 55:
			case 103:
				updateScreen("7");
				break;
			case 56:
			case 104:
				updateScreen("8");
				break;
			case 57:
			case 105:
				updateScreen("9");
				break;

			// Decimal point/comma (international support)
			case 44:
			case 46:
				updateScreen(".");
				break;

			// Backspace clears screen
			case 8:
				clearScreen();
				break;

			// Delete key clears everything
			case 0:
				clearEverything();
				break;

			// Operators
			case 43:
				cacheOp("+");
				break;
			case 45:
				cacheOp("-");
				break;
			case 42:
				cacheOp("*");
				break;
			case 47:
				cacheOp("/");
				break;

			// Equal sign
			case 13:
				calculate();
				break;
			default:
				// alert("Press a number");
				console.log("Keycode: " + e.which);
		}
	});

	function updateScreen(num) {
		if (inputNum.length < 9) {
			// After the equal sign is pressed at the end of an operation, reset cache on pressing a digit
			if (cache[0] !== undefined && operator === "") {
				cache = [];
			}

			if (num == "0" && inputNum[0] === "0") {
				$(".result").text("0");
				return;
			}
			else if (num != "0" && num != "." && inputNum[0] === "0" && inputNum[1] != ".") {
				inputNum[0] = num;
				$(".result").text(num);
				return;
			}
			else if (num == "." && inputNum.indexOf(".") != -1) {
				return;
			}
			else if (num == "." && inputNum.length === 0) {
				inputNum.push("0");
				$(".result").text("0.");
			}
			clearMessage();
			inputNum.push(num);
			$(".result").text(inputNum.join(''));
		}
		else {
			showMessage("Digit limit reached");
		}
	}

	function cacheOp(op) {
		if (inputNum[0] === undefined && operator === "") {
			operator = op;
			return 0;
		}
		if (cache[0] !== undefined && operator !== "" && inputNum[0] === undefined) {
			showMessage("Please, type a number");
			return;
		}
		operator = op;
		if (inputNum[0] !== undefined && cache[0] !== undefined) {
			calculateChain();
			console.log("Both are empty!");
			return 0;
		}
		cache = inputNum;
		inputNum = [];
	}

	function calculate() {
		if (operator === "") {
			showMessage("Operator is not set");
			return;
		}
		result = opList[operator](cache, inputNum);
		if (result.toString().length >= 9) {
			if (result.toString() < 1000) {
				$(".result").text(+result.toFixed(6));
			}
			else {
				$(".result").text("0");
				showMessage("Digit limit reached");
			}
		}
		else {
			$(".result").text(result);
		}
		cache = result.toString().split('');
		inputNum = [];
		operator = "";
	}

	function calculateChain() {
		result = opList[operator](cache, inputNum);
		$(".result").text(result);
		cache = result.toString().split('');
		inputNum = [];
	}

	function clearScreen() {
		inputNum = [];
		$(".result").text("0");
		clearMessage();
	}
	 function clearEverything() {
		clearScreen();
		cache = [];
		result = 0;
		inputNum = [];
		operator = "";
	 }

	function clearMessage() {
		if (messageShown) {
			$(".message").text("");
			messageShown = false;
		}
	}

	function showMessage(message) {
		$(".message").text(message);
		messageShown = true;
	}
});
